from GEODE.Store.Corpus import corpus, Directory, SelfContained
from GEODE.Store.Tabular import tabular, toTSV
import os
import os.path

def prepare(path):
    if '/' in path:
        os.makedirs(os.path.dirname(path), exist_ok=True)
    return path
