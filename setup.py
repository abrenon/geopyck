#!/usr/bin/env python3

from setuptools import find_packages, setup

setup(name='geopyck',
      version='0.1',
      packages=find_packages(),
      entry_points={
          'console_scripts': [
              'geopyck = GEODE:geopyckCLI'
              ]
          })
