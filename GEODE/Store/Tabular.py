from GEODE.Functional import curry
from numpy import vectorize
import pandas

@curry
def toStrKey(areParagraphs, row):
    key = "{work}_{volume:02d}_{article:04d}"
    if areParagraphs:
        key += "_{paragraph:04d}"
    return key.format(**row)

def forPanda(data, f):
    return vectorize(lambda i: f(data.loc[i]))

def toTSV(filePath, data, sortBy='toStrKey'):
    if type(data) != pandas.DataFrame:
        data = pandas.DataFrame(data)
    if sortBy == 'toStrKey':
        sortBy = toStrKey('paragraph' in data)
    if sortBy is None:
        sortedData = data
    else:
        sortedData = data.sort_index(key=forPanda(data, sortBy))
    sortedData.to_csv(filePath, sep='\t', index=False)

def tabular(filePath, **kwargs):
    sep = ',' if filePath[-4:] == '.csv' else '\t'
    return pandas.read_csv(filePath, sep=sep, **kwargs)
