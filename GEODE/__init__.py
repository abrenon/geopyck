"""
geopyck is a set of python tools developed for project GEODE
<https://geode-project.github.io/>
Copyright (C) 2024 Alice BRENON <alice.brenon@ens-lyon.fr>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

from argparse import ArgumentParser
from GEODE.Metadata import article, articleKey, paragraph, paragraphKey, \
        fromKey, relativePath, toKey, uid
from GEODE.Metadata.Domains import superdomains
from GEODE.Store import corpus, Directory, SelfContained, tabular, toTSV
from GEODE.Visualisation import densityProfile, heatmap
from GEODE.Visualisation.ConfusionMatrix import drawConfusionMatrixCLI
from GEODE.Visualisation.DensityProfile import drawDensityProfileCLI

commands = {
        'confusionMatrix': drawConfusionMatrixCLI,
        'densityProfile': drawDensityProfileCLI
        }

def geopyckCLI():
    cli = ArgumentParser(
            prog='geopyck',
            description='A corpus linguistics tool for project GEODE')
    cli.add_argument('command', choices=commands.keys())
    mainArgs, otherArgs = cli.parse_known_args()
    commands[mainArgs.command](otherArgs)
