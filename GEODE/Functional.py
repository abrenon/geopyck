def curry(f):
    return lambda x: (lambda *args: f(x, *args))
