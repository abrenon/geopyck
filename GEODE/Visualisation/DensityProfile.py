import argparse
from GEODE.Signal import resample
from GEODE.Store import prepare, tabular
import matplotlib.pyplot as plot
import seaborn

def gate(measure):
    first, last = measure['position'], measure['position'] + measure['size']
    return [1 if i >= first and i < last else 0
            for i in range(1, 1 + measure['totalSize'])]

def plotDensity(profile, outputPath):
    plot.figure(figsize=(16,13))
    l = len(profile)
    ax = seaborn.lineplot(x=[100*i/(l-1) for i in range(l)], y=profile)
    ax.set_xlabel("Position")
    ax.set_xlim(0, 100)
    ax.set_ylim(0)
    ax.xaxis.set_major_formatter('{x}%')
    ax.set_ylabel("Density")
    ax.yaxis.set_major_formatter('{x}%')
    curve = ax.lines[0]
    x, y = curve.get_xydata()[:,0], curve.get_xydata()[:,1]
    ax.fill_between(x, y, alpha=0.3)
    plot.savefig(prepare(outputPath), dpi=300, bbox_inches='tight')

def sumProfiles(sameSizeProfiles):
    return list(map(sum, zip(*sameSizeProfiles)))

def densityProfile(measures, resolution):
    bySize, count = {}, 0
    for measure in measures:
        distribution = gate(measure)
        count += measure['size']
        l = len(distribution)
        if l not in bySize:
            bySize[l] = []
        bySize[l].append(distribution)
    resampled = map(resample(resolution), map(sumProfiles, bySize.values()))
    return [resolution*x/count for x in sumProfiles(list(resampled))]

def drawDensityProfile(measures, outputFile, resolution):
    plotDensity(densityProfile(measures, resolution), outputFile)

def drawDensityProfileCLI(arguments):
    cli = argparse.ArgumentParser(
            prog='densityProfile',
            description="Draw a density profile from a set of occurrences")
    cli.add_argument('inputTSV')
    cli.add_argument('outputPNG')
    cli.add_argument('-r', '--resolution', type=int)
    args = cli.parse_args(arguments)
    measures = [m[1] for m in tabular(args.inputTSV).iterrows()]
    drawDensityProfile(measures, args.outputPNG, args.resolution or 100)
