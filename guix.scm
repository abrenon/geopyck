(use-modules ((gnu packages machine-learning) #:select (python-pytorch python-scikit-learn))
             ((gnu packages python-science) #:select (python-pandas))
             ((gnu packages python-xyz) #:select (python-matplotlib
                                                  python-nltk
                                                  python-seaborn))
             (guix gexp)
             (guix git-download)
             ((guix licenses) #:select (lgpl3+))
             (guix packages)
             (guix build-system pyproject))

(let
  ((%source-dir (dirname (current-filename))))
  (package
    (name "python-geopyck")
    (version "0.1.0")
    (source
      (local-file %source-dir
                  #:recursive? #t
                  #:select? (git-predicate %source-dir)))
    (build-system pyproject-build-system)
    (propagated-inputs
      (list python-matplotlib
            python-pandas
            python-scikit-learn
            python-seaborn))
    (arguments
     (list #:tests? #f))
    (home-page "https://gitlab.liris.cnrs.fr/geode/geopyck")
    (synopsis "Python tools developed for project GEODE")
    (description
      "Geopyck is a python library to handle a corpus of encyclopedic texts and
generate visualisations.

It was developed as part of project GEODE
@url{https://geode-project.github.io/}")
    (license lgpl3+)))
