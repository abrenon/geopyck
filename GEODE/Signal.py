from GEODE.Functional import curry
import math

@curry
def orientedIntersection(l, sNew, sOld):
    left = max(sNew*l[0], sOld*l[1])
    right = min((sNew+1)*l[0], (sOld+1)*l[1])
    return max(right-left, 0)

@curry
def resample(newSize, distribution):
    oldSize = len(distribution)
    lcm = math.lcm(newSize, oldSize)
    intersection = orientedIntersection((lcm/newSize, lcm/oldSize))
    ratio = oldSize / newSize
    for i in range(newSize):
        yield oldSize/lcm*sum([distribution[j]*intersection(i, j)
                               for j in range(math.floor(i*ratio),
                                              round((i+1)*ratio))])
