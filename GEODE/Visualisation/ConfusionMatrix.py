import argparse
from GEODE.Store import prepare, tabular
import matplotlib.pyplot as plot
import pandas
import seaborn
from sklearn.metrics import confusion_matrix

def trim(name, maxSize):
    if len(name) > maxSize:
        components = name.split(' ')
        return components[0] + ' […]'
    else:
        return name

def trimLabels(labels, maxWidth):
    return labels if maxWidth is None else [trim(l, maxWidth) for l in labels]

def heatmap(matrix, filePath, labels, **kwargs):
    plot.figure(figsize=(16,13))
    ax = seaborn.heatmap(
            matrix, xticklabels=labels, yticklabels=labels, **kwargs
        )
    plot.savefig(prepare(filePath), dpi=300, bbox_inches='tight')

def fromList(data, labels):
    truth = [d['truth'] for d in data]
    if labels is None:
        labels = sorted(set(truth))
    return truth, [d['answer'] for d in data], labels

def fromDataFrame(data, labels):
    truth = data['truth']
    if labels is None:
        labels = sorted(truth.unique())
    return truth, data['answer'], labels

def prepareData(data, labels=None):
    if type(data) == list:
        return fromList(data, labels)
    elif type(data) == pandas.DataFrame:
        return fromDataFrame(data, labels)
    else:
        msg = "Unsupported data format {f} to represent a confusion matrix"
        raise Exception(msg.format(f=type(data)))

def drawConfusionMatrix(data, outputFile, labels=None, maxWidth=None, **kwargs):
    truth, answers, labels = prepareData(data, labels=labels)
    matrix = confusion_matrix(truth, answers, labels=labels, normalize='true')
    heatmap(matrix, outputFile, trimLabels(labels, maxWidth), **kwargs)

def getArgs(arguments):
    cli = argparse.ArgumentParser(
            prog='confusionMatrix',
            description="Draw a confusion matrix from the result of a prediction")
    cli.add_argument('inputTSV')
    cli.add_argument('outputPNG')
    cli.add_argument('-l', '--labels',
                     help="path to a file containing one label per line")
    cli.add_argument('-w', '--maxWidth', type=int,
                     help="length from which labels will be truncated")
    cli.add_argument('-c', '--cmap', help="color map to use")
    return cli.parse_args(arguments)

def drawConfusionMatrixCLI(arguments):
    args = getArgs(arguments)
    data = tabular(args.inputTSV)
    if args.labels is not None:
        with open(args.labels, 'r') as labelsFile:
            labels = list(map(lambda x: x.strip(), labelsFile))
    else:
        labels = None
    drawConfusionMatrix(data,
                        args.outputPNG,
                        labels=labels,
                        maxWidth=args.maxWidth,
                        cmap=args.cmap)
